Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ippsample
Upstream-Contact: Michael Sweet <msweet@apple.com>
Source: http://istopwg.github.io/ippsample/

Files: *
Copyright: 2014-2018 by the IEEE-ISTO Printer Working Group.
  2007-2018 by Apple Inc.
  1997-2007 by Easy Software Products
  2016 by Michael R Sweet.
  2021 by OpenPrinting.
  2020 by The Printer Working Group.
License: Apache-2.0

Files: examples/onepage-letter.ps
       examples/document-a4.ps
Copyright: 1996-2011 Glyph & Cog, LLC
License: GPL-2 or GPL-3
Comment: the code snippet in the ps files is copied from xpdf

Files: vcnet/dns_sd.h
Copyright: 2003-2004, Apple Computer, Inc. All rights reserved.
License: BSD-3

Files: vcnet/regex/*
Copyright: 1992, 1993, 1994, 1997 Henry Spencer
License: not-bsd

Files: cups/*
Copyright: 2006 Jelmer Vernooij
License: krb
Comment: The Kerberos support code ("KSC") is copyright 2006 by Jelmer Vernooij and is
         provided 'as-is', without any express or implied warranty.  In no event will the
         author or Apple Inc. be held liable for any damages arising from the use of the
         KSC.
         .
         Sources files containing KSC have the following text at the top of each source
         file:
         .
             This file contains Kerberos support code, copyright 2006 by Jelmer Vernooij.
         .
         The KSC copyright and license apply only to Kerberos-related feature code in
         CUPS.  Such code is typically conditionally compiled based on the present of the
         HAVE_GSSAPI preprocessor definition.

Files: cups/md5.c cups/md5-internal.h
Copyright: 1999 Aladdin Enterprises
License: aladin

Files: debian/*
Copyright: 2018 Till Kamppeter <till.kamppeter@gmail.com> 
           2021 Thorsten Alteholz <debian@alteholz.de>
License: Apache-2.0
Comment: Debian packaging is licensed under the same terms as upstream

License: Apache-2.0
 /usr/share/common-licenses/Apache-2.0

License: GPL-2
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2'.

License: GPL-3
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file `/usr/share/common-licenses/GPL-3'.

License: not-bsd
 This software is not subject to any license of the American Telephone
 and Telegraph Company or of the Regents of the University of California.
 .
 Permission is granted to anyone to use this software for any purpose on
 any computer system, and to alter it and redistribute it, subject
 to the following restrictions:
 .
 1. The author is not responsible for the consequences of use of this
    software, no matter how awful, even if they arise from flaws in it.
 .
 2. The origin of this software must not be misrepresented, either by
    explicit claim or by omission.  Since few users ever read sources,
    credits must appear in the documentation.
 .
 3. Altered versions must be plainly marked as such, and must not be
    misrepresented as being the original software.  Since few users
    ever read sources, credits must appear in the documentation.
 .
 4. This notice may not be removed or altered.

License: BSD-3
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1.  Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
 2.  Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
 3.  Neither the name of Apple Computer, Inc. ("Apple") nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY APPLE AND ITS CONTRIBUTORS "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL APPLE OR ITS CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: krb
 Permission is granted to anyone to use the KSC for any purpose, including
 commercial applications, and to alter it and redistribute it freely, subject to
 the following restrictions:
 .
   1. The origin of the KSC must not be misrepresented; you must not claim that
      you wrote the original software. If you use the KSC in a product, an
      acknowledgment in the product documentation would be appreciated but is not
      required.
   2. Altered source versions must be plainly marked as such, and must not be
      misrepresented as being the original software.
   3. This notice may not be removed or altered from any source distribution.

License: aladin
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.
