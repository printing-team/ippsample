Source: ippsample
Priority: optional
Section: net
Maintainer: Debian Printing Team <debian-printing@lists.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>,
		Till Kamppeter <till.kamppeter@gmail.com>
Build-Depends: debhelper-compat (= 13),
		avahi-daemon,
		avahi-utils,
		libavahi-client-dev,
		libgnutls28-dev,
		libjpeg-dev,
		libnss-mdns,
		libpng-dev,
		zlib1g-dev,
		libmupdf-dev,
		libcups2-dev
Homepage: https://istopwg.github.io/ippsample/
Standards-Version: 4.6.1
Rules-Requires-Root: binary-targets
Vcs-Browser: https://salsa.debian.org/printing-team/ippsample
Vcs-Git: https://salsa.debian.org/printing-team/ippsample.git

Package: ippsample
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, ippsample-data, libcups2
Conflicts: cups-ipp-utils
Multi-Arch: foreign
Description: samples/development tools for the IPP
 The ippsample project provides sample implementations of an IPP
 Client, Proxy, Server, and other tools. It is based on the CUPS
 source code and is maintained by the PWG IPP workgroup.

Package: ippsample-data
Architecture: all
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}
Recommends: ippsample (>= ${source:Version})
Conflicts: cups-ipp-utils
Description: samples/development tools for the IPP (data)
 The ippsample project provides sample implementations of an IPP
 Client, Proxy, Server, and other tools. It is based on the CUPS
 source code and is maintained by the PWG IPP workgroup.
 .
 This package contains files for /usr/share
